Spring Boot in AWS
==================

Sample project to demonstrate how to deploy Spring Boot application in AWS using
Elastic Beanstalk, and how to add logs, metrics, alarms and dashboard for service
monitoring.