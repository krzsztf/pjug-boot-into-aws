package org.pjug.bootintoaws;

import static io.micrometer.core.instrument.config.MeterFilter.denyUnless;
import static java.lang.Math.round;

import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.Meter.Type;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.config.NamingConvention;
import java.util.Date;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class BootIntoAwsApplication {

  private static final Logger log = LoggerFactory.getLogger(BootIntoAwsApplication.class);

  public static void main(String[] args) {
    SpringApplication.run(BootIntoAwsApplication.class, args);
  }

  @Bean
  MeterRegistryCustomizer<MeterRegistry> meterCustomizer(
      @Value("${my.beanstalk.environment.name}") String environment) {
    log.info("EnvironmentName: {}", environment);
    return registry ->
        registry
            .config()
            .meterFilter(denyUnless(id -> id.getName().matches("http.*|" + environment + "/.*")));
  }

  static class PrependEnvironment implements NamingConvention {
    private final String environment;

    PrependEnvironment(String environment) {
      this.environment = environment;
    }

    @Override
    public String name(String name, Type type, String baseUnit) {
      return environment + "/" + name;
    }
  }

  @RestController
  static class RandomNumberController {
    private static final Logger log = LoggerFactory.getLogger(RandomNumberController.class);

    private final Random random = new Random(new Date().getTime());
    private final LogMetrics metrics;
    private final DistributionSummary summary;
    private final long stddev;

    private long mean;

    RandomNumberController(
        LogMetrics metrics,
        @Value("${my.random.mean}") long mean,
        @Value("${my.random.stddev}") long stddev,
        @Value("${my.beanstalk.environment.name}") String environment,
        MeterRegistry registry) {
      log.info("EnvironmentName: {}", environment);
      this.metrics = metrics;
      this.mean = mean;
      this.stddev = stddev;
      this.summary = registry.summary(environment + "/RandomSummary");
    }

    @GetMapping("/")
    public ResponseEntity<Long> random() {
      long r = round(random.nextGaussian() * stddev + mean);
      metrics.record("Random", r);
      summary.record(r);
      return ResponseEntity.ok(r);
    }

    @PostMapping("/")
    public void mean(@RequestBody long mean) {
      this.mean = mean;
    }
  }

  @Service
  static class LogMetrics {
    private static final Logger log = LoggerFactory.getLogger(LogMetrics.class);

    public void record(String metricName, long metricValue) {
      log.info("MetricName {} MetricValue {}", metricName, metricValue);
    }
  }
}
